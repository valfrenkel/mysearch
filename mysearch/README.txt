There are descriptions of the changes in Mysearch module:

1. mysearch_permission: wrong code formatting

2. mysearch_menu:
  $items['mysearch/%']
  ...
  page arguments' => [1],

There is was implemented changes to allow search any word in URL for mysearch_searchpage() function.

3. mysearch_searchpage:

replaced code:

function mysearch_searchpage() {
  $searchterm = arg(1);
  $query = "
     SELECT nid
     FROM node_revisions
     WHERE body LIKE '%$searchterm%'
     ";
  $search_results = db_query($query);
  $search_results = "<h2>Search for $searchterm</h2><ul>";
  foreach($get_node_result as $record)) {
    $node = node_load($record->nid);
    $search_results .= '<li><a href="/node/'
     . $node->nid . '" title="' . $node->title . '">'
     . $node->title . '</a><!-- debugging output: '
     . print_r($node) . '  --><\li>';
  }
  return $search_results;
}
new code:

function _mysearch_searchpage($search_term) {
  $search_term = trim($search_term);

  $query = db_select('field_data_body', 'n')
    ->fields('n', ['revision_id'])
    ->condition('n.body_value', '%' . $search_term . '%', 'like');

  $results = $query->execute()->fetchAllAssoc('revision_id');
  drupal_set_title(t('Search for @search_term', ['@search_term' => $search_term]));

  return theme('mysearch_results', ['search_term' => $search_term, 'search_results' => $results]);
}

There is was fixed mysql query with new PDO query, also was created the separated template to theme search list.

