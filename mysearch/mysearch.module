<?php

/**
 * Implementation of hook_permission().
 */
function mysearch_permission() {
  return array(
    'access mysearch' => array(
      'title' => t('Access My Search'),
      'description' => t('Allows a user to access search results'),
    )
  );
}

/**
 * Implementation of hook_menu().
 */
function mysearch_menu() {
  $items['mysearch/%'] = array(
    'title' => t('Search'),
    'page callback' => '_mysearch_searchpage',
    'page arguments' => [1],
    'access arguments' => array('access mysearch'),
    'type' => MENU_SUGGESTED_ITEM,
  );
  return $items;
}

/**
 * Menu callback provides a simple list of nodes matching the
 * search term Example: hitting the URL:
 *   http://domain.com/mysearch/example
 * will return a list of links to nodes which have the word
 * example in them.
 *
 * @param string $search_term
 * @return mixed
 */
function _mysearch_searchpage($search_term) {
  $search_term = trim($search_term);

  $query = db_select('field_data_body', 'n')
    ->fields('n', ['revision_id'])
    ->condition('n.body_value', '%' . $search_term . '%', 'like');

  $results = $query->execute()->fetchAllAssoc('revision_id');
  drupal_set_title(t('Search for @search_term', ['@search_term' => $search_term]));

  return theme('mysearch_results', ['search_term' => $search_term, 'search_results' => $results]);
}

/**
 * Implements hook_theme().
 */
function mysearch_theme() {
  return array(
    'mysearch_results' => array(
      'variables' => array('search_term' => NULL, 'search_results' => []),
      'path' => drupal_get_path('module', 'mysearch') . '/templates',
      'template' => 'mysearch-results',
     ),
  );
}

/**
 * Preprocess for mysearch-results.tpl.php
 */
function mysearch_preprocess_mysearch_results(&$variables) {
  foreach ($variables['search_results'] as $key => $result) {
    $node = node_load($result->revision_id);
    $variables['search_results'][$key]->link = l($node->title, 'node/' . $result->revision_id);
  }
}