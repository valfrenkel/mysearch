<?php
/**
 * @file
 * Returns the HTML for search results.
 */
?>
<ul>
  <h2><?php print t('Search results for '); ?><b><?php print $search_term; ?></b> :</h2>
  <?php foreach ($search_results as $result): ?>
      <li>
          <?php print $result->link; ?>
        </li>
    <?php endforeach; ?>
</ul>